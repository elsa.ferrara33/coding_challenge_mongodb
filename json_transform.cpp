
#include "json_transform.h"

json_transform::json_transform()
{
            //create an empty json_transform
}

json json_transform::transform(json in)
{
    json result; //define the return json 
    json_add (in,result,""); //call json_add with an empty string as the base
    return result; 
}

void json_transform::json_add(json j_in, json& j_out, std::string base)
{
    for (auto it = j_in.begin(); it != j_in.end(); ++it) //iteration of the json object
    {
        std::string new_key = base + it.key(); //add the current key to the base

        if(it.value().type_name() == "object") //check if the value is non-terminal
        {
            std::string new_base = new_key + '.'; //add a dot to the new_base
            json_add(it.value(),j_out, new_base); //call json_add with it.value() as j_in and new_base as base | the function is call while the value is non-terminal(with the if condition)
        }
        else
        {
            j_out[new_key] = it.value(); // write in j_out the value associated to new_key, which correspond to the path to the terminal value
        }
    }
}