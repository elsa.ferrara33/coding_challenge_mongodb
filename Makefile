CC=g++
EXEC=exec


all: $(EXEC)

exec: json_transform.o main.o
	$(CC) -o $@ $^ 

main.o: json_transform.h

%.o: %.c
	$(CC) -o $@ -c $< 

clean:
	rm -rf *.o