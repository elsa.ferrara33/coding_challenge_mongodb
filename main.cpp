#include <iostream>
#include <fstream>
#include <string>
#include "nlohmann/json.hpp"
#include "json_transform.h"


using json = nlohmann::json;


int main()
{   
    

    json j_in; //define the input json
    json j_out; //define the output json

    std::cin >> j_in; //take the standard input
    
    std::ofstream o("res.json"); //define the output file

    json_transform jt; //define the transformation
    j_out = jt.transform(j_in); //write in j_out the tranformation of j_in

    o << std::setw(4) << j_out << std::endl; //write j_out in the output file
    std::cout << std::setw(4) << j_out << std::endl; //print j_out 

    return 0;
}






