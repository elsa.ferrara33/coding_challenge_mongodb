#ifndef JSON_TRANSFORM
#define JSON_TRANSFORM

#include "nlohmann/json.hpp"

using json = nlohmann::json;

// json_transform class declaration
class json_transform  
{
    public:
        json_transform(); //constructor definition
        json transform(json in); //member function definition

    private:
        void json_add(json j_in, json& j_out, std::string base); //transformation fonction definition
};

#endif
